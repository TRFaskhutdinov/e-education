import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GroupTimetable} from './timetable-view/groupTimetable';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  // private url = 'https://localhost:65071/api/';
  // private url = 'https://localhost:44322/api/';
  private url = 'https://localhost:5001/api/';
  constructor(private http: HttpClient) { }

  startComputing(expireDate: number): Observable<any> {
    return this.http.post(this.url + 'start', expireDate);
  }

  getTable(): Observable<GroupTimetable[]> {
    return this.http.get<GroupTimetable[]>(this.url + 'gettable');
  }

  saveTable(): Observable<any> {
    return this.http.get(this.url + 'savetable');
  }

  stopComputing(): Observable<any> {
    return this.http.get(this.url + 'stop');
  }

  test(): Observable<any> {
    return this.http.get<any>(this.url + 'test');
  }
}
