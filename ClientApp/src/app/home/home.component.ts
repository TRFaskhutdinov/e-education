import { Component, OnInit } from '@angular/core';
import {GroupTimetable} from '../timetable-view/groupTimetable';
import {HttpService} from '../http.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private httpService: HttpService) { }
minutes: number;
lessons = {
  value: GroupTimetable[1]
};
status = {
  value: ''
};
  ngOnInit() {
    this.status.value = 'start';
  }

  generateFunc() {
    this.status.value = 'await';
    this.minutes = 20;
    this.httpService.startComputing(this.minutes).subscribe(resp =>
         this.updateHandler(1),
        resp => console.log(resp));
  }

  getTableFunc(st, service: HttpService, func1, func2, fout) {
    if (st.value === 'obtained') {
      st.value = 'await';
    }
    if (st.value !== 'start') {
      service.getTable().subscribe(resp => func1(resp, st, service, func1, func2, fout), resp => console.log(resp));
    }
  }

  catchTableFunc(param: GroupTimetable[], st, service: HttpService, func1, func2, fout) {
    if (param == null) {
      setTimeout(func2, 4000, st, service, func1, func2, fout);
    } else {
      fout.value = param;
      st.value = 'obtained';
    }
  }

  saveThisTableFunc() {
    this.status.value = 'completed';
    this.httpService.saveTable().subscribe(resp => this.stopFunc(), resp => console.log(resp));
  }

  stopFunc() {
    this.status.value = 'start';
    this.httpService.stopComputing().subscribe(resp => console.log(resp), resp => console.log(resp));
    this.lessons.value = [];
  }

  updateHandler(num: number) {
    switch (num) {
      case 1: {
        this.getTableFunc(this.status, this.httpService, this.catchTableFunc, this.getTableFunc, this.lessons);
        break;
      }
      case 2: {
        this.saveThisTableFunc();
        break;
      }
      case 3: {
        this.stopFunc();
        break;
      }
    }
  }

  setAwait() {
    this.status.value = 'await';
  }

  testFunc() {
    this.httpService.test().subscribe((resp: string) => console.log(resp));
  }
}
