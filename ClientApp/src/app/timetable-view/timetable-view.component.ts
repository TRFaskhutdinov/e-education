import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GroupTimetable} from './groupTimetable';
import {Lesson} from './lesson';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-timetable-view',
  templateUrl: './timetable-view.component.html',
  styleUrls: ['./timetable-view.component.css']
})
export class TimetableViewComponent implements OnInit {

  @Input() tables: GroupTimetable[];
  @Input() sts;
  @Output() tableFunc = new EventEmitter<number>();

  constructor(private httpService: HttpService) { }

  ls: Lesson[] = [
    {day: 'Пн', auditory: '101', subjNum: 1, subjName: 'math', teacher: 'Yorick'},
    {day: 'Вт', auditory: '101', subjNum: 1, subjName: 'math', teacher: 'Yorick'},
    {day: 'Ср', auditory: '101', subjNum: 1, subjName: 'math', teacher: 'Yorick'},
    {day: 'Чт', auditory: '101', subjNum: 1, subjName: 'math', teacher: 'Yorick'},
    {day: 'Пт', auditory: '101', subjNum: 1, subjName: 'math', teacher: 'Yorick'}
  ];

  groupTimetables: GroupTimetable[] = [
    {group: '1A', lessons : this.ls},
    {group: '1B', lessons : this.ls},
    {group: '1C', lessons : this.ls}
  ];


  ngOnInit() {
  }

  getTableFunc() {
    this.tableFunc.emit(1);
  }

  saveThisTableFunc() {
    this.tableFunc.emit(2);
  }

  stopFunc() {
    this.tableFunc.emit(3);
  }
}
