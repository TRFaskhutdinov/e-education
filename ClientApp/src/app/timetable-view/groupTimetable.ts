import {Lesson} from './lesson';

export class GroupTimetable {
  group: string;
  lessons: Lesson[];
}
