export class Lesson {
  day: string;
  subjNum: number;
  subjName: string;
  teacher: string;
  auditory: string;
}
