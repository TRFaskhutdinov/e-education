import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { TimetableViewComponent } from './timetable-view/timetable-view.component';
import {HttpService} from './http.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TimetableViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  //   RouterModule.forRoot([
  //     { path: '', component: HomeComponent, pathMatch: 'full' }])
   ],
  providers: [HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
