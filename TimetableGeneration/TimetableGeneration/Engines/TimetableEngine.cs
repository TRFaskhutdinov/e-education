﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using TimetableGeneration.Controllers;
using TimetableGeneration.Models.Common;
using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Dto;
using TimetableGeneration.Models.Interfaces;
using TimetableGeneration.Models.TimetableGen;
using TimetableGeneration.Models.TimetableGen.InnerRestrictions;

namespace TimetableGeneration.Engines
{
    public class TimetableEngine
    {
        #region Fields&Properties

        //public DateTime ExpireTime { get; set; }

        private ConcurrentDictionary<Teacher, int> TeacherDuty { get; set; }
        private ConcurrentDictionary<IGroup, int> GroupDuty { get; set; }

        public TimetableObj Sample { get; private set; }
        private readonly Curriculum _curriculum;
        private readonly List<Auditory> _auditories;
        private readonly List<Teacher> _teachers;
        private readonly List<Student> _students;
        private readonly List<IGroup> _groups;
        private static int _dayNum = 6;
        private static int _subjNum = 9;
        private static double _cutScoreModifierModifier = 20.0d;
        public static readonly int CutScoreBase = Assembly
            .GetExecutingAssembly().GetTypes()
            .Where(x => x.GetInterfaces().Any(x1 => x1 == typeof(IRestriction)))
            .Count(x12 => !x12.IsInterface);

        public static double CutScoreModifier
        {
            get => _cutScoreModifierModifier;
            set
            {
                if ((value < 0) || (value > 100))
                    throw new ArgumentOutOfRangeException("Value should be between 0 and 100", (Exception) null);
                _cutScoreModifierModifier = value;
            }
        }

        public static double CutScore => _cutScoreModifierModifier * CutScoreBase / 100;

        public static int DayNum
        {
            get => _dayNum;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException("Should be more than 0", (Exception) null);
                _dayNum = value;
            }
        }

        public static int SubjNum
        {
            get => _subjNum;
            set
            {
                if (value <= 0) throw new ArgumentOutOfRangeException("Should be more than 0", (Exception) null);
                _subjNum = value;
            }
        }

        #endregion

        #region Public methods

        public TimetableEngine(FormDto formDto, Curriculum curriculum, List<Auditory> auditories,
            List<Teacher> teachers, List<Student> students, List<IGroup> groups)
        {
            _curriculum = curriculum;
            //ExpireTime = formDto.ExpireTime;
            StaticData.TablePool.Clear();
            _auditories = auditories;
            _teachers = teachers;
            _students = students;
            _groups = groups;
            TeacherDuty = new ConcurrentDictionary<Teacher, int>();
            GroupDuty = new ConcurrentDictionary<IGroup, int>();

            CreateSample(formDto);
        }

        public void CreateSample(FormDto formDto)
        {
            Sample = new TimetableObj(formDto, _curriculum, _students);
            Sample.ComputeAgilities(this);
        }

        public TimetableObj GetNewTimetableVar(IEnumerable<double> chroms)
        {
            var newTable = (TimetableObj) Sample.Clone();
            newTable.Restrictions = newTable.Restrictions.Zip(chroms, (delegate(IRestriction restriction, double weight)
            {
                restriction.Weight = weight;
                return restriction;
            })).ToList();
            return newTable;
        }

        public TimetableObj GetNewTimetableVar(List<IRestriction> restrictions)
        {
            var newTable = (TimetableObj)Sample.Clone();
            newTable.Restrictions = restrictions;
            return newTable;
        }

        public async Task ProcessTimetable(TimetableObj timetableObj, CancellationToken token)
        {
            //timetableObj.Lessons = timetableObj.Lessons.FirstOrDefault().TimetableObj.Lessons;
            foreach (var lesson in timetableObj.Lessons)
            {
                lesson.Process(this, timetableObj);
            }

            //timetableObj.Lessons.Sort((a, b) => a.TimetableItem.Time.CompareTo(b.TimetableItem.Time));
            timetableObj.GroupLessons();
            timetableObj.ComputeQuality(Sample.Restrictions);
            if (!Gap.NoGaps(timetableObj))
            {
                timetableObj.Quality /= 100;
            }

            //TimetableController.log.AppendLine("Final line!!!\n");

            if (timetableObj.Quality > CutScore)
            {
                //TimetableController.log.AppendLine($"TOTAL  {timetableObj.Gaps.TotalCount}");
                StaticData.TablePool.Enqueue(timetableObj);
                await Task.Run(() => StaticData.AddToPool(timetableObj, token), token);
            }
        }

        public void SetNewWeights(List<IRestriction> restrictions)
        {
            Sample.Restrictions = restrictions;
        }

        public int GetTeacherDuties(Teacher teacher)
        {
            if (!TeacherDuty.TryGetValue(teacher, out var duties))
            {
                duties = _curriculum.CurriculumRecords
                    .Where(x => x.SubjectPlan.Teacher == teacher).Sum(x => x.SubjectPlan.Count);
                TeacherDuty.AddOrUpdate(teacher, duties, (teacher1, i) => duties);
            }

            return duties;
        }

        public int GetGroupDuties(IGroup group)
        {
            if (!GroupDuty.TryGetValue(group, out var duties))
            {
                duties = _curriculum.CurriculumRecords.Where(x => x.Group == group).Sum(x => x.SubjectPlan.Count);
                GroupDuty.AddOrUpdate(group, duties, (group1, i) => duties);
            }

            return duties;
        }

        public IEnumerable<Auditory> GetAppropriateAuditories(Subject subject)
        {
            return _auditories.Where(x => x.Attributes.IsSupersetOf(subject.RoomAttributes));
        }

        public IRestriction SetWeightFunc(IRestriction restriction, double weight)
        {
            restriction.Weight = weight;
            return restriction;
        }

        #endregion
    }
}
