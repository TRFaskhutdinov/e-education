﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using TimetableGeneration.Models.TimetableGen;
using Population = GeneticSharp.Domain.Populations.Population;

namespace TimetableGeneration.Engines
{
    public class GeneticEngine
    {
        public static int Iter = 1;
        public int PopulationQuantity { get; set; }
        //public double ParentFitness { get; set; }
        private TimetableEngine _timetableEngine;
        //private ConcurrentDictionary<IChromosome, List<double>> dict;

        public GeneticEngine(int populationQuantity, TimetableEngine timetableEngine)
        {
            _timetableEngine = timetableEngine;
            PopulationQuantity = populationQuantity;
            Iter = 1;
        }

        public void ProcessGenetic(CancellationToken token)
        {
            double[] min = { 0.0, 0.0, 0.0, 0.0, 0.0 };
            double[] max = { 1.0, 1.0, 1.0, 1.0, 1.0 };
            int[] totBits = { 64, 64, 64, 64, 64 };
            int[] fract = { 10, 10, 10, 10, 10 };
            var chromosome = new FloatingPointChromosome(min, max, totBits, fract);
            var population = new Population(PopulationQuantity, PopulationQuantity, chromosome);
            var fitness = new FuncFitness((c) =>
            {
                var fc = c as FloatingPointChromosome;
                var values = fc?.ToFloatingPoints();

                var table = _timetableEngine.GetNewTimetableVar(values);
                var t = Task.Run(()=>_timetableEngine.ProcessTimetable(table, token), token);
                //t.Start();
                t.Wait(token);
                //_timetableEngine.ProcessTimetable(table).RunSynchronously();
                return table.Quality;
            });
            var selection = new EliteSelection();
            var crossover = new UniformCrossover(0.5f);
            var mutation = new FlipBitMutation();
            var termination = new FitnessStagnationTermination();

            var ga = new GeneticAlgorithm(population, fitness, selection, crossover, mutation)
            {
                Termination = termination
            };

            ga.Start();
            
            while (!token.IsCancellationRequested)
            {
                Iter++;
                ga.Start();
            }
        }

    }
}
