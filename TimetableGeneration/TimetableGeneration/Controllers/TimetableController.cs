﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Common;
using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Dto;
using TimetableGeneration.Models.Enums;
using TimetableGeneration.Models.Interfaces;
using TimetableGeneration.Models.TimetableGen.InnerRestrictions;
using TimetableGeneration.Views;
using static TimetableGeneration.Models.TimetableGen.StaticData;

namespace TimetableGeneration.Controllers
{
    [Produces("application/json")]
    //[Route("api/[controller]")]
    [ApiController]
    public class TimetableController : ControllerBase
    {
        //public static StringBuilder log = new StringBuilder();
        public static Task F;

        [HttpGet("api/getrestrictions")]
        public ActionResult<TypesDto> GetRestrictions()
        {
            var innTypeList = Assembly.GetExecutingAssembly().GetTypes()
                .Where(x => x.IsSubclassOf(typeof(IInnRestriction))).Select(x => x.Name).ToList();
            var addTypeList = Assembly.GetExecutingAssembly().GetTypes()
                .Where(x => x.IsSubclassOf(typeof(IFormRestriction))).Select(x => x.Name).ToList();
            var dto = new TypesDto() {FormRestrictions = addTypeList, InnRestrictions = innTypeList};

            return Ok(dto);
        }

        [HttpPost("api/start")] //to get?
        public ActionResult<string> StartComputing([FromBody]int expireTime)
        {
            // Todo add db calls, change task to genetic;
            FormDto dto = new FormDto(){ExpireTime = expireTime};

            dto.Restrictions = new List<IRestriction>
                {new Balance(), new Gap(), new PrimaryFist(), new Repeat(), new StartInTheMorning()};
            dto.Restrictions[0].Weight = 0.3d;
            dto.Restrictions[1].Weight = 0.9d;
            dto.Restrictions[2].Weight = 0.8d;
            dto.Restrictions[3].Weight = 0.6d;
            dto.Restrictions[4].Weight = 0.7d;

            GetData(out var curriculum, out var auditories, out var teachers, out var students, out var groups);

            if (curriculum == null)
            {
                return BadRequest("Нет учебного плана на текущий год");
            }

            Engine = new TimetableEngine(dto, curriculum, auditories, teachers, students, groups);
            GenEngine = new GeneticEngine(8, Engine);
            TokenSource = new CancellationTokenSource();
            TokenSource.CancelAfter(dto.ExpireTime * 60000);
//            Task.Run(() => Engine.ProcessTimetable(Engine.GetNewTimetableVar(dto.Restrictions)),
//                TokenSource.Token);
            F = Task.Run(() => GenEngine.ProcessGenetic(TokenSource.Token), TokenSource.Token);
            //Task.Run(PoolCompress, TokenSource.Token);
            Task.Run(() => PoolCompress(TokenSource.Token), TokenSource.Token);
            return Ok();
        }

        [HttpPost("api/restart")]
        public ActionResult<string> Recompute([FromForm] List<IRestriction> restrictions, [FromForm] int expireTime)
        {
            TokenSource.Cancel(false);
            TokenSource =new CancellationTokenSource();
            TablePool.Clear();
            CurrentTable = null;
            Engine.SetNewWeights(restrictions);
            TimetableEngine.CutScoreModifier = 30.0d;
            TokenSource.CancelAfter(expireTime * 60000);
            Task.Run(() => Engine.ProcessTimetable(Engine.GetNewTimetableVar(restrictions), TokenSource.Token),
                TokenSource.Token);
            Task.Run(() => PoolCompress(TokenSource.Token), TokenSource.Token);
            return Ok();
        }

        [HttpGet("api/gettable")]
        public ActionResult<IEnumerable<GroupTimetableView>> GetTimetable()
        {
            TablePool.TryDequeue(out var table);
            //log.Append($"get request ({TablePool.Count}, task status = {(f==null?"-1":f.Status.ToString())}). ");
            if (table != null)
            {
                CurrentTable = table;
                //log.AppendLine("positive\n");
                return Ok(table.GroupedPrimarySubjects.Select(x => new GroupTimetableView(x))
                    .Concat(table.GroupedAddSubjects.Select(x => new GroupTimetableView(x))).ToList());
            }

            //log.AppendLine("negative\n");
            return Ok(null);
        }

        [HttpGet("api/savetable")]
        public ActionResult<string> SaveTimetable()
        {
            if (CurrentTable == null) 
            {
                return NotFound();
            }

            var table = CurrentTable.ConvertToTimetable();
            //Todo save it

            return Ok();
        }

        [HttpGet("api/stop")]
        public ActionResult<string> StopCalculation()
        {
            TokenSource.Cancel(false);
            return Ok("stop");
        }

        [HttpGet("api/test")]
        public ActionResult<string> Test()
        {
            FormDto dto = new FormDto() { ExpireTime = 20 };

            dto.Restrictions = new List<IRestriction>
                {new Balance(), new Gap(), new PrimaryFist(), new Repeat(), new StartInTheMorning()};
            dto.Restrictions[0].Weight = 0.3d;
            dto.Restrictions[1].Weight = 0.9d;
            dto.Restrictions[2].Weight = 0.8d;
            dto.Restrictions[3].Weight = 0.6d;
            dto.Restrictions[4].Weight = 0.7d;

            GetData(out var curriculum, out var auditories, out var teachers, out var students, out var groups);

            if (curriculum == null)
            {
                return BadRequest("Нет учебного плана на текущий год");
            }

            Engine = new TimetableEngine(dto, curriculum, auditories, teachers, students, groups);
            TokenSource = new CancellationTokenSource();
            TokenSource.CancelAfter(dto.ExpireTime * 60000);
            F = Task.Run(() => Engine.ProcessTimetable(Engine.GetNewTimetableVar(dto.Restrictions), TokenSource.Token),
                TokenSource.Token);
            Task.Run(() => PoolCompress(TokenSource.Token), TokenSource.Token);
            return Ok("大丈夫");
        }

        [HttpGet("api/log")]
        public ActionResult<string> Log()
        {
            return Ok();//(log.ToString());
        }

        [HttpGet("api/count")]
        public ActionResult<string> Count()
        {
            return Ok(TablePool?.Count().ToString() 
                      + $" {F.IsCanceled} {TokenSource.IsCancellationRequested} pool {TablePool?.Count} score {TimetableEngine.CutScoreModifier} iter {GeneticEngine.Iter}");
        }

        //[HttpGet]
        //public IActionResult GetСurriculums()
        //{
        //    List<Curriculum> curricula = null;
        //    
            
        //    return Ok(curricula.Select(x=>new {id=x.Id, y}));
        //}
        private void GetData(out Curriculum curriculum, out List<Auditory> auditories,
            out List<Teacher> teachers, out List<Student> students, out List<IGroup> groups)
        {
            List<Subject> subjects = new List<Subject>();
            List<CurriculumRecord> curriculumRecords = new List<CurriculumRecord>();

            auditories = new List<Auditory>();
            auditories.Add(new Auditory() { Id = "1", Number = "101", Attributes = new HashSet<RoomAttribute>() { RoomAttribute.Projector } });
            auditories.Add(new Auditory() { Id = "2", Number = "102", Attributes = new HashSet<RoomAttribute>() { RoomAttribute.Computers } });
            auditories.Add(new Auditory() { Id = "3", Number = "103", Attributes = new HashSet<RoomAttribute>() { RoomAttribute.Physics } });
            auditories.Add(new Auditory() { Id = "4", Number = "104", Attributes = new HashSet<RoomAttribute>() { RoomAttribute.Chemical, RoomAttribute.Projector } });
            auditories.Add(new Auditory() { Id = "5", Number = "105", Attributes = new HashSet<RoomAttribute>() });
            auditories.Add(new Auditory() { Id = "6", Number = "106", Attributes = new HashSet<RoomAttribute>() });

            teachers = new List<Teacher>();
            teachers.Add(new Teacher("1", "T1"));
            teachers.Add(new Teacher("2", "T2"));
            teachers.Add(new Teacher("3", "T3"));
            teachers.Add(new Teacher("4", "T4"));
            teachers.Add(new Teacher("5", "T5"));

            groups = new List<IGroup>();
            groups.Add(new MainClassGroup("1", "1A", teachers[0]));
            groups.Add(new MainClassGroup("2", "1B", teachers[1]));
            groups.Add(new MainClassGroup("3", "1C", teachers[2]));
            groups.Add(new MainClassGroup("4", "1E", teachers[3]));
            groups.Add(new AdditionalClassGroup("5", "U1"));
            groups.Add(new AdditionalClassGroup("6", "U2"));

            students = new List<Student>();
            students.Add(new Student("1", "S1", (MainClassGroup)groups[0], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[4] }));
            students.Add(new Student("2", "S2", (MainClassGroup)groups[0], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[4] }));
            students.Add(new Student("3", "S3", (MainClassGroup)groups[0], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[5] }));
            students.Add(new Student("4", "S4", (MainClassGroup)groups[0], new List<AdditionalClassGroup>()));
            students.Add(new Student("5", "S5", (MainClassGroup)groups[0], new List<AdditionalClassGroup>()));
            students.Add(new Student("6", "S6", (MainClassGroup)groups[1], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[4] }));
            students.Add(new Student("7", "S7", (MainClassGroup)groups[1], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[5] }));
            students.Add(new Student("8", "S8", (MainClassGroup)groups[1], new List<AdditionalClassGroup>()));
            students.Add(new Student("9", "S9", (MainClassGroup)groups[1], new List<AdditionalClassGroup>()));
            students.Add(new Student("10", "S10", (MainClassGroup)groups[1], new List<AdditionalClassGroup>()));
            students.Add(new Student("11", "S11", (MainClassGroup)groups[2], new List<AdditionalClassGroup>()));
            students.Add(new Student("12", "S12", (MainClassGroup)groups[2], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[5] }));
            students.Add(new Student("13", "S13", (MainClassGroup)groups[2], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[5] }));
            students.Add(new Student("14", "S14", (MainClassGroup)groups[2], new List<AdditionalClassGroup> { (AdditionalClassGroup)groups[5] }));
            students.Add(new Student("15", "S15", (MainClassGroup)groups[2], new List<AdditionalClassGroup>()));
            students.Add(new Student("16", "S16", (MainClassGroup)groups[3], new List<AdditionalClassGroup>()));
            students.Add(new Student("17", "S17", (MainClassGroup)groups[3], new List<AdditionalClassGroup>()));
            students.Add(new Student("18", "S18", (MainClassGroup)groups[3], new List<AdditionalClassGroup>()));
            students.Add(new Student("19", "S19", (MainClassGroup)groups[3], new List<AdditionalClassGroup>()));
            students.Add(new Student("20", "S20", (MainClassGroup)groups[3], new List<AdditionalClassGroup>()));

            groups[0].Students = students.Take(5).ToList();
            groups[1].Students = students.Skip(5).Take(5).ToList();
            groups[2].Students = students.Skip(10).Take(5).ToList();
            groups[3].Students = students.Skip(15).Take(5).ToList();
            groups[4].Students = new List<Student>() {students[0], students[1], students[5]};
            groups[5].Students = new List<Student>() { students[2], students[6], students[11], students[12], students[13] };

            subjects.Add(new Subject("1", "math", true, new HashSet<RoomAttribute>()));
            subjects.Add(new Subject("2", "geography", true, new HashSet<RoomAttribute>()));
            subjects.Add(new Subject("3", "chemistry", true, new HashSet<RoomAttribute>() { RoomAttribute.Chemical }));
            subjects.Add(new Subject("4", "literature", true, new HashSet<RoomAttribute>()));
            subjects.Add(new Subject("5", "cs", false, new HashSet<RoomAttribute>() { RoomAttribute.Computers }));
            subjects.Add(new Subject("6", "astronomy", false, new HashSet<RoomAttribute>() { RoomAttribute.Projector }));

            curriculumRecords.Add(new CurriculumRecord("1", groups[0], new SubjectCount("1", subjects[0], 5, teachers[4]))); //
            curriculumRecords.Add(new CurriculumRecord("2", groups[0], new SubjectCount("2", subjects[1], 1, teachers[1])));
            curriculumRecords.Add(new CurriculumRecord("3", groups[0], new SubjectCount("3", subjects[2], 3, teachers[2])));
            curriculumRecords.Add(new CurriculumRecord("4", groups[0], new SubjectCount("4", subjects[3], 4, teachers[3])));
            curriculumRecords.Add(new CurriculumRecord("5", groups[1], new SubjectCount("5", subjects[0], 6, teachers[0])));
            curriculumRecords.Add(new CurriculumRecord("6", groups[1], new SubjectCount("6", subjects[1], 1, teachers[1])));
            curriculumRecords.Add(new CurriculumRecord("7", groups[1], new SubjectCount("7", subjects[2], 3, teachers[2])));
            curriculumRecords.Add(new CurriculumRecord("8", groups[1], new SubjectCount("8", subjects[3], 3, teachers[3])));
            curriculumRecords.Add(new CurriculumRecord("9", groups[2], new SubjectCount("9", subjects[0], 6, teachers[0])));
            curriculumRecords.Add(new CurriculumRecord("10", groups[2], new SubjectCount("10", subjects[1], 1, teachers[1])));
            curriculumRecords.Add(new CurriculumRecord("11", groups[2], new SubjectCount("11", subjects[2], 3, teachers[2])));
            curriculumRecords.Add(new CurriculumRecord("12", groups[2], new SubjectCount("12", subjects[3], 4, teachers[3])));
            curriculumRecords.Add(new CurriculumRecord("13", groups[3], new SubjectCount("13", subjects[0], 5, teachers[4]))); //
            curriculumRecords.Add(new CurriculumRecord("14", groups[3], new SubjectCount("14", subjects[1], 1, teachers[1])));
            curriculumRecords.Add(new CurriculumRecord("15", groups[3], new SubjectCount("15", subjects[2], 3, teachers[2])));
            curriculumRecords.Add(new CurriculumRecord("16", groups[3], new SubjectCount("16", subjects[3], 4, teachers[3])));

            curriculumRecords.Add(new CurriculumRecord("17", groups[4], new SubjectCount("17", subjects[4], 2, teachers[0])));
            curriculumRecords.Add(new CurriculumRecord("18", groups[5], new SubjectCount("18", subjects[5], 1, teachers[1])));

            curriculum = new Curriculum("2019", 2019, curriculumRecords);
        }
    }
}