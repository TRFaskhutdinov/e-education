﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimetableGeneration.Controllers;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Common;
using TimetableGeneration.Models.Enums;

namespace TimetableGeneration.Models.TimetableGen
{
    public class SubjectObj : IEquatable<SubjectObj>
    {
        public TimetableItem TimetableItem { get; set; }
        //public TimetableObj TimetableObj { get; set; }
        public Agility Agility { get; set; }
        public double Quality { get; set; }
        public SubjectObj LinkedSubjectObj { get; set; }
        public bool IsAllocated { get; set; }

        protected SubjectObj() { }

        public SubjectObj(CurriculumRecord curriculumRecord)
        {
            //TimetableObj = timetableObj;
            TimetableItem= TimetableItem.CreateTimetableItem(curriculumRecord);
            Agility = new Agility();
            Quality = 0;
            LinkedSubjectObj = null;
            IsAllocated = false;
        }

        public void ComputeAgility(TimetableEngine engine)
        {
            if (Agility.Value > 0) return;
            var auditoryCount = engine.GetAppropriateAuditories(TimetableItem.Subject).Count();
            var teacherDutiesCount = engine.GetTeacherDuties(TimetableItem.Teacher);
            var groupDutiesCount = engine.GetGroupDuties(TimetableItem.Group);
            Agility.Value = (double)auditoryCount / (teacherDutiesCount * groupDutiesCount);
        }

        public void Process(TimetableEngine engine, TimetableObj parentObj)
        {
            if (IsAllocated) return;
            int bestDay = 1, bestSubj = 0;
            double bestScore = 0;
            Auditory bestAuditory = null;
            var allocatedLessons = parentObj.Lessons.Where(x => x.IsAllocated).ToList();
            //TimetableController.log.Append($" | alloc {allocatedLessons.Count} ///");
            if (!TimetableItem.Subject.SubjectAttributes.Contains(SubjectAttribute.IsDouble))
            {
                // Single lesson
                while (TimetableItem.Time.SetNextTime())
                {
                    if (!IsCompatible(allocatedLessons)) continue;
                    //TimetableController.log.Append($"t {TimetableItem.Time.DayNum}_{TimetableItem.Time.SubjNum}");
                    var bookedAuditories = allocatedLessons
                        .Where(x => x.TimetableItem.Time.IsEqual(TimetableItem.Time))
                        .Select(x => x.TimetableItem.Auditory).ToList();
                    var auditories = engine.GetAppropriateAuditories(TimetableItem.Subject)
                        .Except(bookedAuditories).ToList();
                    var theMostUnspecializedAuditory = auditories.Min(x => x.Attributes.Count);
                    var auditory = auditories.FirstOrDefault(x =>
                        x.Attributes.Count == theMostUnspecializedAuditory);
                    if(auditory == null)
                        continue;
                    SetAuditory(auditory);
                    var tempScore = ComputeQuality(parentObj);
                    //TimetableController.log.Append($"auditory {(auditory.Number)}, score {tempScore}");
                    if (tempScore > bestScore)
                    {
                        //TimetableController.log.Append("Better!");
                        bestScore = tempScore;
                        bestDay = TimetableItem.Time.DayNum;
                        bestSubj = TimetableItem.Time.SubjNum;
                        bestAuditory = auditory;
                    }

                    //TimetableController.log.Append("///");
                    ClearAuditory();
                }
            }
            else
            {
                // Double lesson
                Auditory chosenAuditoryNext = null;
                while (TimetableItem.Time.SetNextTime())
                {
                    if (!IsCompatible(allocatedLessons)) continue;
                    LinkedSubjectObj.TimetableItem.Time = (Time)TimetableItem.Time.Clone();
                    if (!LinkedSubjectObj.TimetableItem.Time.SetNextTimeWithoutDayChange())
                    {
                        continue;
                    }
                    if (!IsCompatible(allocatedLessons)) continue;
                    var bookedAuditories = allocatedLessons
                        .Where(x => x.TimetableItem.Time.IsEqual(TimetableItem.Time))
                        .Select(x => x.TimetableItem.Auditory);
                    var auditories = engine.GetAppropriateAuditories(TimetableItem.Subject)
                        .Except(bookedAuditories).ToList();
                    var theMostUnspecializedAuditory = auditories.Min(x => x.Attributes.Count);
                    var chosenAuditory = auditories.FirstOrDefault(x =>
                        x.Attributes.Count == theMostUnspecializedAuditory);
                    if(chosenAuditory == null)
                        continue;

                    //Next section
                    var bookedAuditoriesNext = allocatedLessons
                        .Where(x => x.TimetableItem.Time.IsEqual(LinkedSubjectObj.TimetableItem.Time))
                        .Select(x => x.TimetableItem.Auditory);
                    var auditoriesNext = engine.GetAppropriateAuditories(LinkedSubjectObj.TimetableItem.Subject)
                        .Except(bookedAuditoriesNext).ToList();
                    if (auditoriesNext.Contains(chosenAuditory))
                    {
                        chosenAuditoryNext = chosenAuditory;
                    }
                    else
                    {
                        var theMostUnspecializedAuditoryNext = auditoriesNext.Min(x => x.Attributes.Count);
                        chosenAuditoryNext = auditoriesNext.FirstOrDefault(x =>
                            x.Attributes.Count == theMostUnspecializedAuditoryNext);
                    }

                    if(chosenAuditoryNext == null)
                        continue;
                    SetAuditory(chosenAuditory);
                    LinkedSubjectObj.SetAuditory(chosenAuditoryNext);
                    var tempScore = ComputeQuality(parentObj) +
                                    LinkedSubjectObj.ComputeQuality(parentObj);
                    if (tempScore > bestScore)
                    {
                        bestScore = tempScore;
                        bestDay = TimetableItem.Time.DayNum;
                        bestSubj = TimetableItem.Time.SubjNum;
                    }

                    ClearAuditory();
                    LinkedSubjectObj.ClearAuditory();
                }

                LinkedSubjectObj.TimetableItem.Time.DayNum = bestDay;
                LinkedSubjectObj.TimetableItem.Time.SubjNum = bestSubj + 1;
                LinkedSubjectObj.IsAllocated = true;
                parentObj.Gaps.Set(LinkedSubjectObj);
                LinkedSubjectObj.SetAuditory(chosenAuditoryNext);
            }

            //TimetableController.log.AppendLine("||| ");
            SetAuditory(bestAuditory);
            Quality = bestScore;
            TimetableItem.Time.DayNum = bestDay;
            TimetableItem.Time.SubjNum = bestSubj;
            //var r = parentObj.Lessons.FirstOrDefault(x => x.Id == Id);
            //TimetableController.log.AppendLine($"{r==null} ");
            IsAllocated = true;
            parentObj.Gaps.Set(this);
        }

        public SubjectObj Clone(TimetableObj timetableObj)
        {
            var newSubjectObj = new SubjectObj
            {
                TimetableItem = (TimetableItem) TimetableItem.Clone(),
                //TimetableObj = timetableObj,
                Agility = Agility,
                Quality = 0,
                IsAllocated=false
            };
            return newSubjectObj;
        }

        public bool Equals(SubjectObj other)
        {
            return ((TimetableItem.Group == other?.TimetableItem.Group) &&
                    (TimetableItem.Subject == other?.TimetableItem.Subject));
        }

        private double ComputeQuality(TimetableObj parentObj)
        {
            var quality = 0.0d;
            foreach (var r in parentObj.Restrictions)
                quality += r.ComputePartialQuality(this, parentObj) * r.Weight;

            return quality;
        }

        private bool IsCompatible(IEnumerable<SubjectObj> allocatedLessons)
        {
            if (!TimetableItem.Teacher.WorkTime.Any(x => x.IsEqual(TimetableItem.Time)))
            {
                //TimetableController.log.Append("Teacher dont work ");
                return false;
            }
            var matchedTimeLessons = allocatedLessons
                .Where(x => x.TimetableItem.Time.IsEqual(TimetableItem.Time)).ToList();
            if (matchedTimeLessons.Any(x => x.TimetableItem.Teacher == TimetableItem.Teacher))
            {
                //TimetableController.log.Append("Teacher is busy ");
                return false;
            }
            if (matchedTimeLessons.Any(x => x.TimetableItem.Group.Students
                                                .Intersect(TimetableItem.Group.Students).Count() != 0))
            {
                //TimetableController.log.Append("Student is busy ");
                return false;
            }

            return true;
        }

        private void SetAuditory(Auditory auditory)
        {
            TimetableItem.Auditory = auditory;
        }

        private void ClearAuditory()
        {
            TimetableItem.Auditory = null;
        }
    }
}
