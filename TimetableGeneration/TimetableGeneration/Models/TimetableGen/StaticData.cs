﻿using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TimetableGeneration.Engines;

namespace TimetableGeneration.Models.TimetableGen
{
    public static class StaticData
    {
        public static ConcurrentQueue<TimetableObj> TablePool { get; set; } = new ConcurrentQueue<TimetableObj>();
        public static CancellationTokenSource TokenSource { get; set; }
        public static TimetableEngine Engine { get; set; }
        public static GeneticEngine GenEngine { get; set; }
        public static int PoolCapacity { get; set; } = 50;
        public static int SleepTime { get; set; } = 3000;
        public static TimetableObj CurrentTable { get; set; }

        public static void AddToPool(TimetableObj timetableObj, CancellationToken token)
        {
            while (TablePool.Count>=PoolCapacity)
            {
                if (token.IsCancellationRequested)
                {
                    return;
                }
                Thread.Sleep(SleepTime);
            }

            TablePool.Enqueue(timetableObj);
        }

        public static Task PoolCompress(CancellationToken token)
        {
            while ((TimetableEngine.CutScoreModifier < 85) && !(token.IsCancellationRequested))
            {
                if (TablePool.Count >= PoolCapacity)
                {
                    while (TablePool.Count(x => x.Quality > TimetableEngine.CutScore) > PoolCapacity / 2)
                    {
                        TimetableEngine.CutScoreModifier += 0.5;
                        if (TimetableEngine.CutScoreModifier > 85)
                        {
                            break;
                        }
                    }
                    var newPool =
                        new ConcurrentQueue<TimetableObj>(TablePool.Where(x => x.Quality > TimetableEngine.CutScore));
                    TablePool = newPool;
                }
                Thread.Sleep(5000);
            }

            if (token.IsCancellationRequested)
            {
                TablePool.Clear();
                TimetableEngine.CutScoreModifier = 20.0d;
            }
            return Task.CompletedTask;
        }
    }
}
