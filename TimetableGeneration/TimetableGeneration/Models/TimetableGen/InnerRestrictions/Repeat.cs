﻿using System.Collections.Generic;
using System.Linq;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.TimetableGen.InnerRestrictions
{
    public class Repeat : IInnRestriction
    {
        public double Weight { get; set; }
        public double ComputePartialQuality(SubjectObj subjectObj, TimetableObj parentObj)
        {
            var count = parentObj.Lessons.Where(x => x.IsAllocated)
                .Where(x => x.Equals(subjectObj))
                .Count(x => x.TimetableItem.Time.DayNum == subjectObj.TimetableItem.Time.DayNum);
            return (double) 1 / (count + 1);
        }

        public double ComputeTimetableQuality(TimetableObj timetableObj)
        {
            double dailyAverCount = (IterateGroups(timetableObj.GroupedPrimarySubjects) +
                                     IterateGroups(timetableObj.GroupedAddSubjects)) / 2;
            return 1.0 / (dailyAverCount + 1);
        }

        private double IterateGroups(List<GroupedSubjectObj> groupedSubjectObjs)
        {
            double count = 0;
            foreach (var groupedSubjectObj in groupedSubjectObjs)
            {
                count += groupedSubjectObj.SubjectObjs.GroupBy(x => x.TimetableItem.Time.DayNum)
                    .Select(x => x.Count() - x.Distinct().Count()).Average();
            }

            count /= (groupedSubjectObjs.Count() * TimetableEngine.DayNum);
            return count;
        }

        public object Clone()
        {
            return new Repeat() {Weight = 0};
        }
    }
}
