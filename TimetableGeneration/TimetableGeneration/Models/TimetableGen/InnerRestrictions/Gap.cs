﻿using TimetableGeneration.Controllers;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.TimetableGen.InnerRestrictions
{
    public class Gap : IInnRestriction
    {
        public double Weight { get; set; }
        public double ComputePartialQuality(SubjectObj subjectObj, TimetableObj parentObj)
        {
            var changes = parentObj.Gaps.Check(subjectObj);
            //TimetableController.log.Append($"gaps ch ={changes}");
            double result = (double) -changes / subjectObj.TimetableItem.Group.Students.Count;
            if (changes <= 0)
            {
                return result;
            }

            // Максиальное количество добавляемых окон зависит от числа предметов в день и числа затронутых студентов.
            return result / (TimetableEngine.SubjNum - 2);

        }

        public double ComputeTimetableQuality(TimetableObj timetableObj)
        {
            return 0;
        }

        public static bool NoGaps(TimetableObj timetableObj) => timetableObj.Gaps.TotalCount == 0;

        public object Clone()
        {
            return new Gap() {Weight = 0};
        }
    }
}
