﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.TimetableGen.InnerRestrictions
{
    public class StartInTheMorning : IInnRestriction
    {
        public double Weight { get; set; }
        public double ComputePartialQuality(SubjectObj subjectObj, TimetableObj parentObj)
        {
            return 1.1 - 0.1 * subjectObj.TimetableItem.Time.SubjNum;
        }

        public double ComputeTimetableQuality(TimetableObj timetableObj)
        {
            double score = 0.0d;
            var students = timetableObj.GroupedPrimarySubjects.SelectMany(x => x.Group.Students).ToList();
            foreach (var student in students)
            {
                score += (double) timetableObj.Lessons.Where(x => x.TimetableItem.Group.Students.Contains(student))
                             .GroupBy(x => x.TimetableItem.Time.DayNum)
                             .Select(x => x.Min(y => y.TimetableItem.Time.SubjNum - 1)).Sum() / TimetableEngine.DayNum;
            }

            score /= students.Count;
            return 1 / (score + 1);
        }

        public object Clone()
        {
            return new StartInTheMorning() {Weight = 0};
        }
    }
}
