﻿using System.Linq;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.TimetableGen.InnerRestrictions
{
    public class PrimaryFist : IInnRestriction
    {
        public double Weight { get; set; }
        public double ComputePartialQuality(SubjectObj subjectObj, TimetableObj parentObj)
        {
            var lessons = parentObj.Lessons.Where(x => x.IsAllocated)
                .Where(x => x.TimetableItem.Time.DayNum == subjectObj.TimetableItem.Time.DayNum).ToList();
            if (subjectObj.TimetableItem.Subject.IsPrimary)
            {
                var additionalLessonBeforeThisScore = lessons.Where(x => !x.TimetableItem.Subject.IsPrimary)
                    .Where(x => x.TimetableItem.Group.Students.Intersect(subjectObj.TimetableItem.Group.Students)
                                    .Count() != 0).Where(x =>
                        x.TimetableItem.Time.SubjNum < subjectObj.TimetableItem.Time.SubjNum).Sum(x =>
                        x.TimetableItem.Group.Students.Intersect(subjectObj.TimetableItem.Group.Students).Count());
                return 1.0 / (additionalLessonBeforeThisScore + 1);
            }

            var primaryLessonAfterThisScore = lessons.Where(x => x.TimetableItem.Subject.IsPrimary)
                .Where(x => x.TimetableItem.Group.Students.Intersect(subjectObj.TimetableItem.Group.Students)
                                .Count() != 0).Where(x =>
                    x.TimetableItem.Time.SubjNum > subjectObj.TimetableItem.Time.SubjNum).Sum(x =>
                    x.TimetableItem.Group.Students.Intersect(subjectObj.TimetableItem.Group.Students).Count());
            return 1.0 / (primaryLessonAfterThisScore + 1);
        }

        public double ComputeTimetableQuality(TimetableObj timetableObj)
        {
            double score = 0.0d;
            foreach (var groupedAddSubject in timetableObj.GroupedAddSubjects)
            {
                foreach (var subjectObj in groupedAddSubject.SubjectObjs)
                {
                    score += timetableObj.GroupedPrimarySubjects
                        .Where(x => x.Group.Students.Intersect(groupedAddSubject.Group.Students).Count() != 0).Sum(x =>
                            x.SubjectObjs
                                .Where(y => y.TimetableItem.Time.DayNum == subjectObj.TimetableItem.Time.DayNum)
                                .Count(y => y.TimetableItem.Time.SubjNum > subjectObj.TimetableItem.Time.SubjNum) *
                            x.Group.Students.Intersect(groupedAddSubject.Group.Students).Count());
                }
            }

            score /= timetableObj.GroupedAddSubjects.Sum(x => x.SubjectObjs.Count);
            return 1.0 / (1 + score);
        }

        public object Clone()
        {
            return new PrimaryFist() {Weight = 0};
        }
    }
}
