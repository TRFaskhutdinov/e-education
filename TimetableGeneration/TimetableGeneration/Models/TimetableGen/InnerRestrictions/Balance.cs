﻿using System;
using System.Linq;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.TimetableGen.InnerRestrictions
{
    public class Balance : IInnRestriction
    {
        public double Weight { get; set; }
        public double ComputePartialQuality(SubjectObj subjectObj, TimetableObj parentObj)
        {
            var lessonCountList = parentObj.Lessons.Where(x => x.IsAllocated)
                .Where(x => x.TimetableItem.Group == subjectObj.TimetableItem.Group)
                .GroupBy(x => x.TimetableItem.Time.DayNum).Select(x => new {day = x.Key, lessonCount = x.Count()})
                .OrderBy(x => x.lessonCount).ToList();
            var index = lessonCountList.FindIndex(x => x.day == subjectObj.TimetableItem.Time.DayNum);
            if (index == -1)
            {
                return 1.0;
            }
            // Пустые дни наиболее предпочтительны, но их не будет в списке.
            var emptyDaysCount = TimetableEngine.DayNum - lessonCountList.Count;
            return 1.0 / (emptyDaysCount + index + 1);
        }

        public double ComputeTimetableQuality(TimetableObj timetableObj)
        {
            // считается среднее арифметическое по группам из среднеквадратичных отклонений по дням для каждой группы
            var averageLessonCountStandardDeviation = timetableObj.GroupedPrimarySubjects
                .Concat(timetableObj.GroupedAddSubjects)
                .Average(x => Math.Sqrt(x.SubjectObjs.GroupBy(x1 => x1.TimetableItem.Time.DayNum).Average(x2 =>
                    (x2.Count() - x.SubjectObjs.GroupBy(x1 => x1.TimetableItem.Time.DayNum).Average(x3 => x3.Count())) *
                    (x2.Count() - x.SubjectObjs.GroupBy(x1 => x1.TimetableItem.Time.DayNum)
                         .Average(x3 => x3.Count())))));

            return 1.0 / (averageLessonCountStandardDeviation + 1);
        }

        public object Clone()
        {
            return new Balance() {Weight = 0};
        }
    }
}
