﻿namespace TimetableGeneration.Models.TimetableGen
{
    public class Agility
    {
        public double Value { get; set; }

        public Agility()
        {
            Value = -1;
        }
    }
}
