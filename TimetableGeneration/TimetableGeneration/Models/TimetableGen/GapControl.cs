﻿using System;
using System.Collections.Generic;
using TimetableGeneration.Controllers;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Common.Users;

namespace TimetableGeneration.Models.TimetableGen
{
    public class GapControl : ICloneable
    {
        public Dictionary<Student, TimeBorders>[] Borders { get; set; }
        public int TotalCount { get; private set; }

        public GapControl() { }

        public GapControl(IEnumerable<Student> students)
        {
            Borders = new Dictionary<Student, TimeBorders>[TimetableEngine.DayNum + 1];
            Borders[0] = new Dictionary<Student, TimeBorders>();
            TotalCount = 0;

            foreach (Student student in students)
            {
                Borders[0].Add(student, new TimeBorders());
            }
            for (int i = 1; i < TimetableEngine.DayNum + 1; i++)
            {
                Borders[i] = new Dictionary<Student, TimeBorders>(Borders[0]);
            }
        }

        public object Clone()
        {
            var newControl = new GapControl
            {
                Borders = new Dictionary<Student, TimeBorders>[TimetableEngine.DayNum + 1],
                TotalCount = 0

            };
            for (int i = 0; i < TimetableEngine.DayNum + 1; i++)
            {
                newControl.Borders[i] = new Dictionary<Student, TimeBorders>(Borders[0]);
            }
            return newControl;
        }

        /// <summary>
        /// Return change of gap's count
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        public int Check(SubjectObj subject)
        {
            var result = 0;
            foreach (var student in subject.TimetableItem.Group.Students)
            {
                var borders = Borders[subject.TimetableItem.Time.DayNum][student];
                if (borders.Max == 0)
                {
                    continue;
                }

                var diff1 = subject.TimetableItem.Time.SubjNum - borders.Max;
                if (diff1 < 1)
                {
                    var diff = subject.TimetableItem.Time.SubjNum - borders.Min;
                    if (diff > 0)
                    {
                        result--;
                    }
                    else
                    {
                        result -= (diff + 1);
                    }
                }
                else
                {
                    result += (diff1 - 1);
                }
            }
            return result;
        }

        /// <summary>
        /// Set change of gap's count
        /// </summary>
        /// <param name="subject"></param>
        public void Set(SubjectObj subject)
        {
            var result = 0;
            foreach (var student in subject.TimetableItem.Group.Students)
            {
                var borders = Borders[subject.TimetableItem.Time.DayNum][student];
                if (borders.Max == 0)
                {
                    borders.Max = subject.TimetableItem.Time.SubjNum;
                    borders.Min = subject.TimetableItem.Time.SubjNum;
                    Borders[subject.TimetableItem.Time.DayNum][student] = borders;
                    continue;
                }

                var diff1 = subject.TimetableItem.Time.SubjNum - borders.Max;
                if (diff1 < 1)
                {
                    var diff = subject.TimetableItem.Time.SubjNum - borders.Min;
                    if (diff > 0)
                    {
                        result--;
                        continue;
                    }
                    else
                    {
                        result -= (diff + 1);
                        borders.Min = subject.TimetableItem.Time.SubjNum;
                    }
                }
                else
                {
                    result += (diff1 - 1);
                    borders.Max = subject.TimetableItem.Time.SubjNum;
                }

                Borders[subject.TimetableItem.Time.DayNum][student] = borders;
            }
//            TimetableController.log.Append(
//                $"gap ch {subject.TimetableItem.Time.DayNum}_{subject.TimetableItem.Time.SubjNum} = {result}/");
            TotalCount += result;
        }
    }
}
