﻿using System.Collections.Generic;
using System.Linq;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.TimetableGen
{
    public class GroupedSubjectObj
    {
        public IGroup Group { get; set; }
        public List<SubjectObj> SubjectObjs { get; set; }
        
        public GroupedSubjectObj(IGrouping<IGroup, SubjectObj> subjects)
        {
            Group = subjects.Key;
            SubjectObjs = subjects.ToList();
            SubjectObjs.Sort((a, b) => a.TimetableItem.Time.CompareTo(b.TimetableItem.Time));
        }
    }
}
