﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Common;
using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Dto;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.TimetableGen
{
    public class TimetableObj : ICloneable
    {
        public List<SubjectObj> Lessons { get; set; }
        public List<GroupedSubjectObj> GroupedPrimarySubjects { get; set; }
        public List<GroupedSubjectObj> GroupedAddSubjects { get; set; }
        public List<IRestriction> Restrictions { get; set; }
        public GapControl Gaps { get; set; }
        public double Quality { get; set; }

        public TimetableObj(FormDto dto, Curriculum curriculum, IEnumerable<Student> students)
        {
            //DoubleLessons= new Dictionary<TimetableItem, int>();
            Lessons= new List<SubjectObj>();
            foreach (var record in curriculum.CurriculumRecords)
            {
                var subjObj = new SubjectObj(record);
               if (subjObj.TimetableItem.Subject.SubjectAttributes.Contains(Enums.SubjectAttribute.IsDouble))
               {
                   if (record.SubjectPlan.Count % 2 != 0) record.SubjectPlan.Count++;
                   List<SubjectObj> tempList=new List<SubjectObj>();
                    for (int i = 0; i < record.SubjectPlan.Count / 2; i++)
                    {
                        tempList.Add(subjObj.Clone(this));
                    }
                    for (int i = 0; i < record.SubjectPlan.Count / 2; i++)
                    {
                        tempList.Add(subjObj.Clone(this));
                        tempList[i].LinkedSubjectObj = tempList[i + record.SubjectPlan.Count / 2];
                        //tempList[i + record.SubjectPlan.Count / 2].LinkedSubjectObj = tempList[i];
                    }
                    Lessons.AddRange(tempList);
               }
               else
               {
                   for (int i = 0; i < record.SubjectPlan.Count; i++)
                   {
                       Lessons.Add(subjObj.Clone(this));
                   }
               }
            }

            Restrictions = dto.Restrictions;

            Quality = 0;
            Gaps=new GapControl(students);
        }

        protected TimetableObj() { }

        public void GroupLessons()
        {
            var byGroups = Lessons.GroupBy(x => x.TimetableItem.Group);
            GroupedPrimarySubjects=new List<GroupedSubjectObj>();
            GroupedAddSubjects= new List<GroupedSubjectObj>();
            foreach (var byGroup in byGroups)
            {
                if (byGroup.FirstOrDefault().TimetableItem.Subject.IsPrimary)
                {
                   GroupedPrimarySubjects.Add(new GroupedSubjectObj(byGroup));
                }
                else
                {
                    GroupedAddSubjects.Add(new GroupedSubjectObj(byGroup));
                }
            }
        }

        public void ComputeAgilities(TimetableEngine engine)
        {
            foreach (var lesson in Lessons) lesson.ComputeAgility(engine);
            Lessons.Sort(delegate (SubjectObj a, SubjectObj b)
            {
                if (a.TimetableItem.Subject.IsPrimary == b.TimetableItem.Subject.IsPrimary)
                {
                    return a.Agility.Value.CompareTo(b.Agility.Value);
                }

                if (a.TimetableItem.Subject.IsPrimary)
                {
                    return -1;
                }

                return 1;
            });
        }

        public void ComputeQuality(IEnumerable<IRestriction> sampleRestrictions)
        {
            Quality = 0.0d;
            foreach (var r in sampleRestrictions)
                Quality += r.ComputeTimetableQuality(this) * r.Weight;
        }

        public Timetable ConvertToTimetable()
        {
            var items = Lessons.Select(x => x.TimetableItem);
            return new Timetable(items);
        }

        public object Clone()
        {
            var newTable = new TimetableObj
            {
                Quality = 0,
                Lessons = new List<SubjectObj>(),
                Gaps = (GapControl) Gaps.Clone(),
                Restrictions = new List<IRestriction>()
            };
            foreach (var lesson in Lessons)
            {
                newTable.Lessons.Add(lesson.Clone(this));
            }

            foreach (var i in Restrictions)
            {
                newTable.Restrictions.Add((IRestriction)i.Clone());
            }

            return newTable;
        }
    }
}
