﻿namespace TimetableGeneration.Models.TimetableGen
{
    public struct TimeBorders
    {
        public int Min;
        public int Max;
    }
}
