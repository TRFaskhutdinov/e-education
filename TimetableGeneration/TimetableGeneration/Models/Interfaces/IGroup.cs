﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TimetableGeneration.Models.Common.Users;

namespace TimetableGeneration.Models.Interfaces
{
    public interface IGroup
    {
        [Key]
        string Id { get; set; }
        string Name { get; set; }
        List<Student> Students { get; set; }
    }
}
