﻿using Microsoft.AspNetCore.Identity;
using TimetableGeneration.Models.Enums;

namespace TimetableGeneration.Models.Interfaces
{
    public abstract class PersonalInf : IdentityUser
    {
        public string FullName { get; set; }
        public Sex Sex { get; set; }

        public string Address { get; set; }

        public PersonalInf() { }
    }
}
