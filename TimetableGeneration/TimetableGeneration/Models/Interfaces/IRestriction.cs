﻿using System;
using TimetableGeneration.Models.TimetableGen;

namespace TimetableGeneration.Models.Interfaces
{
    public interface IRestriction : ICloneable
    {
        double Weight { get; set; }

        double ComputePartialQuality(SubjectObj subjectObj, TimetableObj parentObj);

        double ComputeTimetableQuality(TimetableObj timetableObj);
    }
}
