﻿using System.ComponentModel.DataAnnotations;

namespace TimetableGeneration.Models.Interfaces
{
    public abstract class DbModel
    {
        [Key]
        public string Id { get; set; }
    }
}
