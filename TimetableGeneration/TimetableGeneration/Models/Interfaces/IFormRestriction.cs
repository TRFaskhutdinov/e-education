﻿namespace TimetableGeneration.Models.Interfaces
{
    public interface IFormRestriction : IRestriction
    {
        string Value { get; set; }
    }
}
