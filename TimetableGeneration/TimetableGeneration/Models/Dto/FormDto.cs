﻿using System;
using System.Collections.Generic;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Dto
{
    public class FormDto
    {
        public List<IRestriction> Restrictions { get; set; }
        public int ExpireTime { get; set; }
        // Todo не будет нормально десерелизовываться
    }
}
