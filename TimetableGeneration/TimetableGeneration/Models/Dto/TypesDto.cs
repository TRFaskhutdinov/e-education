﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Dto
{
    public class TypesDto
    {
        public List<string> FormRestrictions;
        public List<string> InnRestrictions;

        public TypesDto()
        {
            InnRestrictions = new List<string>();
            FormRestrictions= new List<string>();
        }
    }
}
