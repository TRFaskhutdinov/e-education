﻿using System.Collections.Generic;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class Curriculum : DbModel
    {
        public int Year { get; set; }
        public virtual List<CurriculumRecord> CurriculumRecords { get; set; }

        public Curriculum()
        {

        }

        public Curriculum(string id, int year, List<CurriculumRecord> records)
        {
            Id = id;
            Year = year;
            CurriculumRecords = records;
        }
    }
}
