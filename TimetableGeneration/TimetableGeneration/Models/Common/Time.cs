﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TimetableGeneration.Engines;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class Time : DbModel, ICloneable, IComparable
    {
        public int DayNum { get; set; }
        public int SubjNum { get; set; }

        [NotMapped]
        public bool IsEnd => ((DayNum == TimetableEngine.DayNum) && (SubjNum == TimetableEngine.SubjNum));

        protected Time()
        {

        }

        internal bool SetNextTime()
        {
            if (SubjNum < TimetableEngine.SubjNum)
            {
                SubjNum++;
            }
            else
            {
                if (DayNum < TimetableEngine.DayNum)
                {
                    SubjNum = 1;
                    DayNum++;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        internal static Time CreateGenTime() => new Time() {DayNum = 1, SubjNum = 0};

        internal void EraseGenTime()
        {
            DayNum = 1;
            SubjNum = 0;
        }

        internal bool IsEqual(Time anotherTime)
        {
            if ((DayNum == anotherTime.DayNum) && (SubjNum == anotherTime.SubjNum))
            {
                return true;
            }

            return false;
        }

        public object Clone()
        {
            Time newTime = new Time {DayNum = DayNum, SubjNum = SubjNum};
            return newTime;
        }

        public bool SetNextTimeWithoutDayChange()
        {
            if (SubjNum >= TimetableEngine.SubjNum) return false;
            SubjNum++;
            return true;
        }

        public int CompareTo(object obj)
        {
            var b = (Time) obj;
            int result= DayNum.CompareTo(b.DayNum);
            if (result == 0)
            {
                return SubjNum.CompareTo(b.SubjNum);
            }

            return result;
        }
    }

}
