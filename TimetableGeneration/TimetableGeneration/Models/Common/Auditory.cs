﻿using System.Collections.Generic;
using TimetableGeneration.Models.Enums;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class Auditory : DbModel
    {
        public string Number { get; set; }
        public virtual HashSet<RoomAttribute> Attributes { get; set; }

        public Auditory()
        {

        }
    }
}
