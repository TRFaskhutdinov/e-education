﻿using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Diary
{
    public class StudentAdvancement : DbModel
    {
        public virtual Student Student { get; set; }
        public virtual Module Module { get; set; }
        public bool IsDone { get; set; }

        protected StudentAdvancement() { }
    }
}
