﻿using System.Collections.Generic;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Diary
{
    public class Diary : DbModel
    {
        public int Year { get; set; }
        public virtual List<Lesson> Lessons { get; set; }
        public virtual List<StudentAdvancement> StudentAdvancements { get; set; }
        public virtual Timetable Timetable { get; set; }

        protected Diary() { }
    }
}
