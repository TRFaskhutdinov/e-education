﻿using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Enums;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Diary
{
    public class Journal : DbModel
    {
        public virtual Student Student { get; set; }
        public virtual Attendance Attendance { get; set; }

        protected Journal() { }
    }
}
