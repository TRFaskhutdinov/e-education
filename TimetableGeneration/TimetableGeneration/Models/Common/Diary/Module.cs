﻿using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Diary
{
    public class Module : DbModel
    {
        public virtual Subject Subject { get; set; }
        public string Description { get; set; }

        protected Module() { }
    }
}
