﻿using System;
using System.Collections.Generic;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Diary
{
    public class Lesson : DbModel
    {
        public virtual TimetableItem Inf { get; set; }
        public DateTime ExecutionTime { get; set; }
        public virtual List<Journal> Journals { get; set; }
        public string Homework { get; set; }

        protected Lesson() { }
    }
}
