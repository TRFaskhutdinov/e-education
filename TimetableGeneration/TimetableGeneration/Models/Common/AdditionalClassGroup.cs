﻿using System.Collections.Generic;
using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class AdditionalClassGroup : DbModel, IGroup
    {    
        public string Name { get; set; }
        public virtual List<Student> Students { get; set; }

        protected AdditionalClassGroup()
        {

        }

        public AdditionalClassGroup(string id, string name)
        {
            Id = id;
            Name = name;
            Students = new List<Student>();
        }
    }
}
