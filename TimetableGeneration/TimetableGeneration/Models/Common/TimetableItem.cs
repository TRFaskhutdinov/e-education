﻿using System;
using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class TimetableItem : DbModel, ICloneable
    {
        public virtual Subject Subject { get; set; }
        public virtual IGroup Group { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual Time Time { get; set; }
        public virtual Auditory Auditory { get; set; }

        protected TimetableItem()
        {

        }

        public static TimetableItem CreateTimetableItem(CurriculumRecord curriculumRecord)
        {
            var item = new TimetableItem
            {
                Subject = curriculumRecord.SubjectPlan.Subject,
                Teacher = curriculumRecord.SubjectPlan.Teacher,
                Group = curriculumRecord.Group,
                Time = Time.CreateGenTime(),
                Auditory = null
            };
            return item;
        }

        public object Clone()
        {
            var newTimetableItem = new TimetableItem
            {
                Subject = Subject,
                Group = Group,
                Teacher = Teacher,
                Auditory = Auditory,
                Time = Time.CreateGenTime()
            };
            return newTimetableItem;
        }
    }
}
