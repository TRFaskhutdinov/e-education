﻿using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class SubjectCount : DbModel
    {
       public virtual Subject Subject { get; set; }
       public int Count { get; set; }
       public virtual Teacher Teacher { get; set; }

       protected SubjectCount()
       {

       }

       public SubjectCount( string id, Subject subject, int count, Teacher teacher)
       {
           Id = id;
           Subject = subject;
           Count = count;
           Teacher = teacher;
       }
    }
}
