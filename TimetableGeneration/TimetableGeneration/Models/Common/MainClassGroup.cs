﻿using System.Collections.Generic;
using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class MainClassGroup : DbModel, IGroup
    {
        public string Name { get; set; }
        public virtual List<Student> Students { get; set; }

        public virtual Teacher ClassTeacher { get; set; }
        public int Grade { get; set; }

        protected MainClassGroup()
        {

        }

        public MainClassGroup(string id, string name, Teacher teacher)
        {
            Id = id;
            Name = name;
            Students= new List<Student>();
            ClassTeacher = teacher;
        }
    }
}
