﻿using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Users
{
    public class Parent : PersonalInf
    {
        public string AdditionalInf { get; set; }

        protected Parent()
        {

        }
    }
}
