﻿using System.Collections.Generic;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Users
{
    public class Student : PersonalInf
    {
        public  string PassportSer { get; set; }
        public string PassportNum { get; set; }
        public string BirthSertNum { get; set; }

        public virtual List<Parent> Parents { get; set; }
        public virtual MainClassGroup Grade { get; set; }
        public virtual List<AdditionalClassGroup> AdditionalGroups{ get; set; }

        protected Student()
        {

        }

        public Student(string id, string name, MainClassGroup mainGroup, List<AdditionalClassGroup> addGroups)
        {
            Id = id;
            FullName = name;
            Grade = mainGroup;
            AdditionalGroups = addGroups;
        }
}
}
