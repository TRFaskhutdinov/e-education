﻿using Microsoft.AspNetCore.Identity;
using TimetableGeneration.Models.Enums;

namespace TimetableGeneration.Models.Common.Users
{
    public class Admin : IdentityUser
    {
        public virtual Duties Duties { get; set; }

        protected Admin() { }
    }
}
