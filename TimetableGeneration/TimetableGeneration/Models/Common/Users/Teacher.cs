﻿using System.Collections.Generic;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common.Users
{
    public class Teacher : PersonalInf
    {
        public virtual List<Subject> Specs { get; set; }
        public virtual List<Time> WorkTime { get; set; }
        public virtual List<Project> Projects { get; set; }
        public int DutyHourCount { get; set; }
        public string Qualification { get; set; }

        protected Teacher()
        {

        }

        public Teacher(string id, string name)
        {
            FullName = name;
            Id = id;
            WorkTime = GetFullTime();
        }

        public List<Time> GetFullTime()
        {
            List<Time> lst=new List<Time>();
            var time = Time.CreateGenTime();
            while (time.SetNextTime())
            {
                lst.Add((Time) time.Clone());
            }

            return lst;
        }
    }
}
