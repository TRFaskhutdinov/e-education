﻿using TimetableGeneration.Models.Common.Users;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class Project : DbModel
    {
        public virtual Teacher Teacher { get; set; }
        public virtual Student Student { get; set; }
        public string Name { get; set; }

        protected Project() { }
    }
}
