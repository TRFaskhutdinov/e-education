﻿using System.Collections.Generic;
using TimetableGeneration.Models.Enums;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class Subject : DbModel
    {
        public string Name { get; set; }
        public bool IsPrimary { get; set; }
        public virtual HashSet<RoomAttribute> RoomAttributes { get; set; }
        public virtual HashSet<SubjectAttribute> SubjectAttributes { get; set; }

        protected Subject()
        {

        }

        public Subject(string id, string name, bool isPrimary, HashSet<RoomAttribute> attributes)
        {
            Id = id;
            Name = name;
            IsPrimary = isPrimary;
            RoomAttributes = attributes;
            SubjectAttributes= new HashSet<SubjectAttribute>();
        }
    }
}
