﻿using System.Collections.Generic;
using System.Linq;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public sealed class Timetable : DbModel
    {
        public List<TimetableItem> Items { get; set; }

        private Timetable()
        {

        }

        public Timetable(IEnumerable<TimetableItem> items)
        {
            Items = items.ToList();
            Items.Sort((a, b) => a.Time.CompareTo(b.Time));
        }
    }
}
