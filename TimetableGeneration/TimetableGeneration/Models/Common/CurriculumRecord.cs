﻿using System.Collections.Generic;
using TimetableGeneration.Models.Common.Diary;
using TimetableGeneration.Models.Interfaces;

namespace TimetableGeneration.Models.Common
{
    public class CurriculumRecord : DbModel
    {
        public virtual IGroup Group { get; set; }
        public virtual SubjectCount SubjectPlan { get; set; }
        public virtual List<Module> Modules { get; set; }

        protected CurriculumRecord()
        {

        }

        public CurriculumRecord(string id, IGroup group, SubjectCount subjectPlan)
        {
            Id = id;
            Group = group;
            SubjectPlan = subjectPlan;
        }
    }
}
