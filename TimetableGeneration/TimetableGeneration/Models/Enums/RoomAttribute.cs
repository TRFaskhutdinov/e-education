﻿namespace TimetableGeneration.Models.Enums
{
    public enum RoomAttribute
    {
        Projector,
        Computers,
        Chemical,
        Physics,
        SportsEq
    }
}
