﻿namespace TimetableGeneration.Models.Enums
{
    public enum Role
    {
        Admin,
        Student,
        Teacher,
        Parent
    }
}
