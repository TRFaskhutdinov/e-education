﻿namespace TimetableGeneration.Models.Enums
{
    public enum Attendance
    {
        Attend,
        Absent,
        Ill
    }
}
