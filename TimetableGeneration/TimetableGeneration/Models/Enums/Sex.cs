﻿namespace TimetableGeneration.Models.Enums
{
    public enum Sex
    {
        Male,
        Female
    }
}
