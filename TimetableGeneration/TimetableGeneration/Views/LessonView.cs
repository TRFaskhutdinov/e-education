﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimetableGeneration.Models.Common;
using TimetableGeneration.Models.TimetableGen;

namespace TimetableGeneration.Views
{
    public class LessonView
    {
        public string day;
        public int subjNum;
        public string subjName;
        public string teacher;
        public string auditory;

        public LessonView(TimetableItem item)
        {
            switch (item.Time.DayNum)
            {
                case 1:
                    day = "Пн";
                    break;
                case 2:
                    day = "Вт";
                    break;
                case 3:
                    day = "Ср";
                    break;
                case 4:
                    day = "Чт";
                    break;
                case 5:
                    day = "Пт";
                    break;
                case 6:
                    day = "Сб";
                    break;
                default:
                    day = "...";
                    break;
            }

            subjNum = item.Time.SubjNum;
            subjName = item.Subject.Name;
            teacher = item.Teacher.FullName;
            auditory = (item.Auditory == null ? "~" : item.Auditory.Number);
        }
    }
}
