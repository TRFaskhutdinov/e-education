﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimetableGeneration.Models.TimetableGen;

namespace TimetableGeneration.Views
{
    public class GroupTimetableView
    {
        public string group;
        public List<LessonView> Lessons;

        public GroupTimetableView(GroupedSubjectObj subjectObj)
        {
            group = subjectObj.Group.Name;
            Lessons = subjectObj.SubjectObjs.Select(x => new LessonView(x.TimetableItem)).ToList();
        }
    }
}
